Dinâmica: As Soluções, o Registro (as Bolhas) e a Rede.


DESCRIÇÃO AMPLA DA ATIVIDADE

Inicialmente, os grupos são separados por classes de personagem (como artistas, políticos, professores) e os diferentes grupos abordam um problema comum sob suas diferentes perspectivas, buscando receber o prêmio de grande vencedor. Neste primeiro momento, os grupos podem se comunicar entre si por meio de um número limitados de postits que são apresentados por um mediador para os outros grupos. A comunicação é estabelecida por meio de trocas: um grupo só envia mensagem se outro também enviar. A proposta elaborada é relatada em uma folha contendo a documentação, a folha 1 (diplomacia de interesse). O que os participantes não saberão é que o mediador apenas passará as informações que for do seu interesse, pondendo transforma-las em fake-news, propagandas ou mesagens sem nenhuma relação as que receberam.

Num segundo momento, é apresentado um novo objetivo para a competição, elaborar uma proposta que melhor contemple mais grupos. Nesse momento, eles continuam com o mesmo modelo de comunicação, que começa a se tornar frustrante. Os grupos são apresentados a uma nova forma de comunicação, um fórum de livre comunicação, onde os grupos postam seu projeto abertamente para todos os outros e enxergam a proposta (conversa abertamente P2P). O participantes terão liberdade para escolher qual meio de comunicação usar: Informação aberta, aonde ambos os grupos que forem conversar deve expor sua proposta inteira (conversa abertamente P2P) ou informaçõs fechadas, os grupos permanecem conversando via post its como referido acima (diplomacia de interesse). Ao final, todos os grupos apresentam as suas propostas, a folha 2.

No momento de anunciar o grande grupo vencedor, a banca examinadora manifesta que os grupos que colaboraram tiveram propostas mais contempladoras e completas. É anunciado que o grande grupo vencedor são todos os grupos com relação a si mesmos quando estavam fechados nos seus contextos. Então é lançado o desafio de todos os grupos elaborarem em conjunto uma terceira proposta, que é melhor que suas primeiras e segundas propostas, e para que todos ganhem, todos devem melhorar suas propostas para contemplar todos os grupos, caso um grupo nao tenha uma proposta melhor que a sua propria anterior entao todos sairao perdendo. Nessa etapa, a comunicação é livre entre os grupos, a folha 3.


*Facilitação:* 11 pessoas

* Cerimonialista/Impulsianor de projetos das Nações Unidas
** Atribuição: Fazer falas ao grande grupo, conduzir orientações em cada um dos intervalos, dar o compasso do evento, articular as pessoas da monitoria com relação ao tempo.
* Algoritmo
** Atribuição: Levar mensagens de um grupo a outro, cuidar quanto a fluxos de informação não planejados entre os grupos, distribuir identificadores de grupo.
* Interface de compartilhamento
** Atribuição: Orientar grupos que quiserem compartilhar seus projetos, garantir abertura completa da informação, cuidar tempo nesse espaço, distribuir identificadores de grupo.
* Monitoria/Referência da área (1 por grupo)
** Atribuição: Orientações iniciais por grupo, provocar um pouco da identidade do grupo, orientar cada grupo quanto ao tempo, cuidar quanto a fluxos de informação não planejados entre os grupos, distribuir identificadores de grupo.


*Cronograma detalhado:* Total (115 minutos) mais Preparo (40 minutos)

-1: Articulação das pessoas facilitadoras (30 minutos): Grupo conversa e alinha detalhes sobre a dinâmica, resolvendo todas as dúvidas, assim cada pessoa deve ter claramente sua atribuição em cada etapa e o tempo para cada uma delas
0. Formação dos grupos (10 minutos): As pessoas da monitoria, junto com a interface e o algoritmo distribuem os identificadores de grupo a cada um dos participantes. Inicialmente não é explicado o que significa o identificador, apenas que será importante para a dinâmica e que cada um deve guardar o seu. O identificador pode ser como uma pulseira, bandana ou crachá. Se for pulseira ou bandana o ideal é que seja de pano, mas uma alternativa barata é papel celofane colorido. Já se for um crachá é importante que tenha um papel bem colorido dentro para sinalizar a qual grupo a pessoa faz parte.
# Abertura (10 minutos): Participantes já estão acomodados e com identificador em mãos. A pessoa cerimonialista abre a dinâmica e se apresenta como impulsionador de projetos da ONU. Essa pessoa contextualiza os desafios da implementação dos 17 Objetivos para o Desenvolvimento Sustentável (ODS) e como o ODS 17 é chave para alavancar todos os outros, e portanto precisamos de uma nova parceiria global para interação entre pessoas e grupos, que atualmente estão vulneráveis às tecnologias digitais, que por sua vez muitas vezes não atende demandas da sociedade, e as pessoas e grupos não tem autonomia com relação a essas tecnologias e interfaces, o que tem gerado crises políticas e humanitárias pelo mundo. Então o facilitador oferece um grande prêmio ao setor da sociedade que oferecer a melhor solução ao problema ou à pergunta: "Como eliminar a vulnerabilidade das pessoas na rede, para que possamos fazer uma nova parceria global e colaborar para todos os objetivos do desenvolvimento sustentável?" A seguir ele informa o tempo que os participantes terão para elaborar a solução e apresenta o kit que cada um tem (duas folhas especiais, com a solução, uma folha de papel vegetal, e folhas e canetinhas para os esboços). Orienta para que guardem os esboços, pois vão ser avaliados também e que a solução seja escrita com o papel vegetal embaixo e mais outra folha especial embaixo, para backup. Enfim informa que os identificadores são para definir o grupo ou setor da sociedade à qual a pessoa participa e que sigam a pessoa monitora de seu grupo, que também terá um identificador e as conduzirá até a ilha de trabalho do grupo.
# Aquecimento dos grupos (10 minutos): Cada monitor garante que todas as pessoas chegaram até si e então conduz o seu grupo para sua ilha de trabalho, que deve ser um aglomerado de mesas com cadeiras para todo mundo e um kit de trabalho. Alternativamente o trabalho pode ser feito em um círculo no chão se for avaliado que há condições de trabalho. Então a referência deve dizer a qual setor da sociedade este grupo corresponde e deve apresentar quais são as características do grupo, espaços de fala/expressão/poder, espaço de interlocução com outros setores, potencialidades e algumas limitações. Focar nas potencialidades ao invés de limitações. O objetivo é provocar o espírito de que eles são representates desse setor no grupo. Deve também orientar para a definição de um mediador e um relator. O mediador deve focar em garantir que todas as pessoas do grupo tenham garantida a sua fala. E a relatora é quem faz as anotações de solução, garantindo a documentação. Deve possibilitar que outras pessoas também registrem, mas deve zelar pela folha de documentação. Ao final, pergunta-se se ainda restam dúvidas e tenta saná-las.
# Primeira rodada (20 minutos): Nesse período cada grupo elabora a sua solução internamente no grupo. As referências ficam próximas a cada grupo para verificar se está tudo correndo bem, cuidam o tempo e se colocam para resolver dúvidas pontuais. Cerimonialista, inferface e algoritmo ficam atentas e atentos às referências, caso precisem de suporte. Ao final do período, as referências começam a avisar que o tempo está se esgotando.
# Primeiro intervalo (10 minutos): Nesse momento, cerimonialista interrompe os trabalhos de maneira enérgica e faz um comunicado. Ao invés de avaliar os projetos neste momento, será dada uma prorrogação. Em conversas com outros grupos de trabalho da ONU, surgiu a ideia de que mais importante do que uma boa solução de um dos grupos é uma solução que contemple todos os setores da sociedade. Então, o grupo que receberá o grande prêmio será o que melhor contemplar a todas as equipes. Para isso, serão dadas duas opções, a comunicação pelo mensageiro e a da interface de compartilhamento. Então é dado tempo para o mensageiro e para a interface se apresentarem. Elas apresentam as condições de cada uma das opções. Enfim inicia-se a segunda rodada.
# Segunda rodada (20 minutos): As referências entregam novas folhas de documentação para cada grupo. Durante os 15 minutos iniciais, estará aberto o espaço de compartilhamento. O mensageiro estará circulando pelos grupos, oferecendo a oportunidade de participar de sua plataforma, mediante concordância com os termos e condições de uso. Já  a interface estará aguardando em um espaço distante as pessoas chegarem. Na entrada, ela garantirá sob quais termos a pessoa quer compartilhar o conteúdo. E então mediará as interações, garantindo que as dúvidas e respostas estão sendo registradas. Nos últimos 5 minutos, mensageiro e interface interromperão as interações e o foco será no registro da documentação.
# Segundo intervalo (20 minutos): Nesse momento a pessoa mediadora de cada grupo apresenta o projeto em dois minutos. A pessoa cerimonialista faz anotações e discute brevemente com outras facilitadoras. Ao final, dá o veredicto, elogiando os aspectos de colaboração dos grupos que decidiram compartilhar todos os seus materiais, e apontando dificuldades ou limitações de colaboração daqueles que interagiram por meio do mensageiro. Então, fala que todas os grupos devem ter uma oportunidade de interagirem entre si e melhorarem suas propostas com relação a si mesmos. Assim todas as equipes serão vencedoras, caso contrário, não.
# Última rodada (20 minutos): Um novo conjunto de folhas de documentação é distribuído. Nesse momento, os grupos interagem livremente entre si. Referências ajudam cada grupo a manter foco, acompanhamento do tempo e já fazem análise preliminar do trabalho.
# Fechamento (5 minutos): A pessoa cerimonialista faz anúncio enérgico do final dos trabalhos e anuncia que as referências já indicaram avanços em todos os grupos, portanto todos são vencedores. Para receber o prêmio, todas as pessoas são reunidas em um círculo em que faremos uma atividade de sintonia, relaxamento e preparação chamada pula 8. O prêmio simbólico é o final do pula 8, o que será explicado pela pessoa cerimonialista.


*Recursos:*

* Mesas para os grupos: alternativamente pode se trabalhar no chão se avaliar-se que é possível.
* Cadeiras para todos os participantes: a não ser que o trabalho seja no chão.
* Palco: a partir de 20 participantes.
* Microfone: a partir de 50 participantes.
* Objetos identificadores de grupo: Idealmente pano colorido para ser usado como bandana ou pulseira. Uma alternativa é um crachá com folha colorida. Uma última seria tiras de papel celofane
* Folhas A3 coloridas: 6 por grupo.
* Folhas A3 vegetais: 2 por grupo.
* Folhas de rascunho: diversas por grupo.
* Canetas coloridas: Um conjunto por grupo
* Post its: 2 blocos para a interface e 4 para o mensageiro
* Folhas de orientação para cada uma das 11 pessoas facilitadoras
* Cronômetro
* Termos e Condições de Uso do Mensageiro
* Cartilhas das licenças Creative Commons
* Folhas indicativas das mesas de trabalho e da plataforma de compartilhamento


Duração: 1 hora e 55 minutos.


*Objetivos:*

* Estimular a colaboração entre pessoas e grupos
* Realizar a transição do competitivo para o colaborativo.
* Promover a valorização de opiniões e pontos de vista diversas


*Infraestrutura necessária:*

*  Sala ampla, quadra ou ginásio com algumas ilhas de classes dispostas ao longo de sua extensão e cadeiras. A atividade pode ser realizada em espaço aberto.


*Tema:* As midias sociais e tecnologias da informação tem muito poder de manipular as pessoas: como evitar a vulnerabilidade, incentivar o senso critico e pensar próprio 
Este é um tema para mostrar que a colaboratividade e o uso adequado das ferramentas informaticas são fundamentais


*OBJETIVO DO DESENVOLVIMENTO SUSTENTÁVEL 17*. Fortalecer os meios de implementação e revitalizar a parceria global para o desenvolvimento sustentável

* A Assistência Oficial ao Desenvolvimento (OAD) levantou aproximadamente 135 bilhões de dólares em 2014.
* Em 2014, 79% dos produtos de países em desenvolvimento entraram no mercado “duty-free” de países desenvolvidos.
* A dívida dos países em desenvolvimento continua estável, beirando 3% do rendimento de exportação.
* O número de usuários da internet na África quase dobrou nos últimos quatro anos.
* Em 2015, 95% da população mundial tem cobertura de sinal de celular.
* 30% da juventude mundial é de nativos digitais, ativos online por pelo menos cinco anos.
* A população mundial apresentou aumento do uso da internet de 6% em 2000 para 43% em 2015.
* No entanto, mais de 4 bilhões de pessoas não usam Internet, e 90% delas são de países em desenvolvimento.
* *Este objetivo tem como caracteristica integrar todos os ODS da ONU.*


*Método:*

* Partir de uma competição em divisão por grupos
* Colaboração entre cada grupo
* Utilização de um método que remeta a internet
* Incitar a ter um intermediador e registrador por grupo


*Quem somos nós?*

O Centro de Tecnologia Acadêmica do Instituto de Física da UFRGS (CTA IF/UFRGS) é um laboratório e comunidade de prática com objetivo de integrar e fortalecer novos paradigmas para produção e disseminação do conhecimento, voltado para a liberdade e autonomia das pessoas. Buscamos reunir Hardware Aberto e Livre, Ciência Aberta, Educação Aberta, Software Livre, Ciência Cidadã, dentre outras formas de aproximar as pessoas do conhecimento científico e tecnológico e promover relações para a emancipação tecnológica.

A EITCHA!, a Escola Itinerante de Tecnologia Cidadã Hacker, é uma iniciativa para promover a emancipação tecnológica em escolas. Fazemos isso por meio de ciclos de oficinas sobre diferentes aspectos sobre tecnologias livres e por meio de atividades educacionais colaborativas, como mutirões de montagem de Estações Meteorológicas Modulares. Em 2017, fomos agraciados com o prêmio global Mozilla Minigrant.

Os estudantes serão reunidos em grupos que simulem papeis sociais distintos, como professores, legisladores, artistas, empresários para chegarem numa solução para um dos objetivos do desenvolvimento sustentavel da ONU.  A partir de então serão provocados a registrarem a solução encontrada da melhor forma, compartilharem suas soluções e exercerem a reflexão crítica sobre diferentes elementos da tecnologia digital e suas interações com a sociedade.
