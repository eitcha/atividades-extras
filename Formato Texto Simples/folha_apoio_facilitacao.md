ORIENTAÇÕES PARA FACILITADORAS E FACILITADORES


*Pessoa Facilitadora:* Cerimonialista/Impulsionador de projetos das Nações Unidas

*Atribuições gerais:*
    
    Conduzir a atividade e fazer falas ao grande público. Marcar o compasso da atividade e facilitar para os facilitadores. Essa é uma função que requer dois atributos importantes: visão clara da dinâmica e capacida de falar de maneira enfática para grandes grupos. Portanto, é recomendado que quem assuma esse papel esteja bem familiarizado com a dinâmica e consiga falar para grandes grupos, sem ser prolixo.

*Atribuições por etapa:*
    
-1: Articulação das pessoas facilitadoras (30 minutos): Garanta que o microfone e o cronômetro está funcionando. Procure as demais pessoas da facilitação para perguntar se restam dúvidas. Garanta que tens anotado as referências responsáveis por cada grupo e a cor correpondente. Ensaie suas falas, pois elas precisam ser bem precisas em tempo, ou ajude a pessoas da facilitação que estiver com mais demandas de organização caso sobre tempo.
0. Formação dos grupos (10 minutos): Prepare-se para falar e fique atento às respostas das outras facilitadoras.
# Abertura (10 minutos): Abra a dinâmica com bastante ênfase e apresente-se como impulsionador de projetos da ONU. Contextualize os desafios da implementação dos 17 Objetivos para o Desenvolvimento Sustentável (ODS) e como o ODS 17 é chave para alavancar todos os outros,  de que é necessária uma nova parceiria global para interação entre pessoas e grupos, atualmente vulneráveis às tecnologias digitais, que por sua vez muitas vezes não atende demandas da sociedade. As pessoas e grupos não tem autonomia com relação a essas tecnologias e interfaces, o que tem gerado crises políticas e humanitárias pelo mundo. Então ofereça um grande prêmio (nomeado apenas "Grande Prêmio", algo misterioso mesmo) ao setor da sociedade que oferecer a melhor solução ao problema ou à pergunta: "Como eliminar a vulnerabilidade das pessoas na rede, para que possamos fazer uma nova parceria global e colaborar para todos os objetivos do desenvolvimento sustentável?" Informa que os participantes terão 20 minutos para elaborar a solução e apresenta o kit que cada um tem (duas folhas especiais, com a solução, uma folha de papel vegetal, e folhas e canetinhas para os esboços). Diga que a folha especial é onde será registrada a solução, o que será explicado mais a fundo pelas referências. Orienta para que guardem também os esboços, pois vão ser avaliados também e para que a solução seja escrita com o papel vegetal embaixo e mais outra folha especial embaixo, para backup. Enfim informa que os identificadores são para definir o grupo ou setor da sociedade à qual a pessoa participa e que sigam a pessoa monitora de seu grupo, que também terá um identificador e as conduzirá até a ilha de trabalho do grupo.
# Aquecimento dos grupos (10 minutos): Fique atento aos monitores e ao cronômetro. Circule pelos grupos e pergunte se algum facilitador ou facilitadora precisa de ajuda, se sentir que é o caso.
# Primeira rodada (20 minutos): Fique atento para dar suporte, pois esse tende a ser um momento mais intenso. Também marque o tempo e quando faltarem dois minutos se prepare para fazer a primeira interrupção.
# Primeiro intervalo (10 minutos): Interrompa os trabalhos de maneira enérgica e faça um comunicado. Ao invés de avaliar os projetos neste momento, será dada uma prorrogação. Em conversas com outros grupos de trabalho da ONU, surgiu a ideia de que mais importante do que uma boa solução de um dos grupos é uma solução que contemple todos os setores da sociedade. Então, o grupo que receberá o grande prêmio será o que melhor contemplar a todas as equipes. Para isso, serão dadas duas opções, a comunicação pelo mensageiro e a da interface de compartilhamento. Passe a palavra para a interface e depois ao mensageiro para se apresentarem. Quando encerrarem, dê início a segunda rodada e diga que há mais 20 minutos de trabalho.
# Segunda rodada (20 minutos): Fique muito atenta ou atento nessa etapa, pois todas as outras pessoas da facilitação estarão com funções específicas e somente tu fará parte do suporte. Nos últimos 2 minutos, volte a se preparar para a nova interrupção.
# Segundo intervalo (20 minutos): Chame as mediadoras de cada grupo para irem até o palco apresentarem as soluções. Faça anotações e discute brevemente com outras facilitadoras. Preste atenção a detalhesem que a colaboração ajudou, e tende encontrar brechas onde ela poderia ter ajudado ainda mais. Ao final, dê o veredicto. Busque elogiar os aspectos de colaboração dos grupos que decidiram compartilhar todos os seus materiais, e aponte dificuldades ou limitações de colaboração daqueles que interagiram por meio do mensageiro. Então, diga que todas os grupos devem ter uma oportunidade de interagirem entre si e melhorarem suas propostas com relação a si mesmos. Assim todas as equipes serão vencedoras, caso contrário, não. Indique o último período de 20 minutos.
# Última rodada (20 minutos): Circule pelos grupos observando os avanços e as interações. Converse com cada referência para ver quais foram os avanços dos grupo quando chegar próximo ao final da rodada. As conversas devem ser breves, pois não há muito tempo. Faça anotações com os pontos chaves para incluir na próxima fala.
# Fechamento (5 minutos): Faça anúncio enérgico do final dos trabalhos e anuncie que as referências já indicaram avanços em todos os grupos, portanto todos são vencedores. Cite alguns desses avanços, de preferência para cada grupo, colhidos nas conversas e anotações. Reúna todas as pessoas em um círculo para receber o prêmios e uma atividade de sintonia, relaxamento e preparação chamada pula 8. Indique como o pula 8 funciona. Todas e todos devem pular relaxando e movendo seus membros em cada pulo. Na primeira vez devem pular contando de 8 até 1, depois se viram e repetem, então se viram e pulam de 7 até 1, se viram novamente e repetem, e assim por diante, até chegar ao número 1. No final se viram para o centro e gritam liberando energia e cargas do processo. Diga que o prêmio chegará para todes ao final do pula 8. O prêmio simbólico é o final do pula 8, no qual todas e todos compatilharam de um momento emocionante construído por todas e todos.


*Referências:* Comece mais a la anunciador de lutas marciais e termine mais com uma pegada de grande liderança mundial, como Ghandi, Bob Marley, Mandela ou outro de sua preferência. Com isso remete-se a ideia de começar pela competitividade e termina-se mais pela colaboratividade.

*Frase de apoio:* Em caso de dúvida, pegue as frases abaixo como referência.

* Abertura: "Olá! Sejam todas bem-vindas e bem-vindo ao Grande Desafio do Desenvolvimento Sustentável. Eu sou fulana/fulano de tal, e nas Nações Unidas sou responsável por impulsionar projetos para o desenvolvimento sustentável. Por isso, vim aqui a um país em desenvolvimento do Sul Global, o Brasil para convidar grandes representantes de diferentes setores da sociedade para proporem soluções para um grande problema que temos. E é por isso que estão aqui, o que muito me honra. Ainda mais que os 17 Objetivos do Desenvolvimento Sustentável que temos no mundo agora, desde áreas alimentação à saúde e meio ambiente, temos uma dificuldade de colaborar mundialmente. Precisamos de uma nova parceria global, o que faz parte do ODS 17. As pessoas estão vulneráveis à tecnologia digital e incapazes de aproveitar melhor seu potencial de colaboração, podendo ser conduzidas justamente na direção contrária dos 17 objetivos, como por meio do aprofundamento da violência e tensionamento político nesses países. Portanto, convidamos a todos os grupos a responderem: 'Como eliminar a vulnerabilidade das pessoas na rede, para que possamos fazer uma nova parceria global e colaborar para todos os objetivos do desenvolvimento sustentável?' A melhor solução será agraciada com o Grande Prêmio! Vocês terão 20 minutos para elaborarem as soluções dentro do seu setor e registrar isso de forma documentada. Afinal, se não há uma memória, um registro do trabalho que fizeram, não temos como seguir adiante e é como se não tivesse existido. Para isso vocês terão essas folhas especiais, na qual ficará o registro definitivo. Escrevam com o papel vegetal abaixo para garantir o backup do trabalho de vocês. As demais folhas são o registro de desenvolvimento de vocês, e também serão avaliados, portanto guardem. Vocês receberam um objeto que identifica a qual grupo fazem parte. E cada grupo terá uma referência para dar suporte para vocês. Assim que eu terminar de falar, quem recebeu a cor x vai até a fulana de tal...(siga até cobrir todos os grupos e referências). Boa sorte rumo ao Grande Prêmio! Podem começar!"
* Primeiro intervalo: "Olá! Que belo trabalho de desenvolvimento! Agora é hora de parar. Sei que disse que quem elaborasse a melhor solução faturaria o grande prêmio, mas estive conversando com outros grupos da Nações Unidas e realmente nessa conversa mais aberta ficou claro que não faz sentido simplesmente encontrar a melhor solução, mas sim a que melhor contempla todos os setores da sociedade. Para isso, vocês terão a oportunidade de interagir entre os grupos por meio de plataformas: a interface e o mensageiro. (Tempo para a fala da interface e do mensageiro). Então, com essa novas oportunidades, repondam à nossa questão:  'Como eliminar a vulnerabilidade das pessoas na rede, para que possamos fazer uma nova parceria global e colaborar para todos os objetivos do desenvolvimento sustentável?' E concorram ao Grande Prêmio! Qual será o melhor grupo? O mais representativo? Mãos à obra!"
* Segundo intervalo. Frase final: "Então vamos ter todas e todos a oportunidade de colaborar plenamente e alcançar a melhor solução dentro da contribuição que podemos dar, contemplando outros setores da sociedade. Temos todos 20 minutos para melhor nossas propostas com relação a nós mesmas no passado, quando não tivemos tanta chance de colaborar. Ao final dessa etapa, se conseguirmos todas e todos avançar, receberemos todas e todos o grande prêmio. Vamos construir as melhores soluções, e essas são aquelas das quais todas e todos participam!"
* Encerramento. Frase final: "Vocês sentiram? Esse foi nosso grande prêmio, esse momento coletivo que construímos, com a colaboração de todas, uma grande Genki Dama. E isso simboliza o que fizemos, construímos soluções colaborativas, pensando em todas e todos. As oportunidades que temos são muito mais do que uma ou outra coisa que ganhamos e adquirimos. Então aproveitem essa grande oportunidade nesse evento. E lembrem-se, ainda mais nesse novo mundo em que vivemos, estar juntas e juntos é fundamental. Colaborar é o caminho. Seja qual for o objetivo. Independentemente do desenvolvimento que estiveres fazendo. E seja qual for a sustentabilidade que estiver buscando. Espero que tenham se divertido tanto quanto nós. Gratidão!"

______________________________

*Pessoa Facilitadora: Interface de compartilhamento*

*Atribuições:*
    
    Essa pessoa deve manter um bom controle do espaço no qual estiver facilitando, conseguindo orientar diversas pessoas ao mesmo tempo. Também deve ter clareza das opções de licenciamento Creative Commons. Passará boa parte da atividade no suporte da dinâmica, então deve ser alguém observador. É alguém com familiaridade intermediária com a atividade.

*Atribuições por etapa:*
    
-1: Articulação das pessoas facilitadoras (30 minutos): Ajudar a preparar o espaço em ilhas de trabalho, com as cadeiras e as mesas, bem como organize o espaço da interface de compartilhamento. Prepare post its Creative Commons para serem aderidos aos trabalhos. Posicione os indicativos de "espaço de publicação" e "fórum de discussões" nesse espaço, e posicione no fórum os post its de perguntas e respostas e os indicativos correspondentes. Ajude as demais pessoas da facilitação e ajuda a resolver dúvidas.
0. Formação dos grupos (10 minutos): Ajude na distribuição dos objetos identificadores de grupo.
# Abertura (10 minutos): Nesse momento, tu e o mensageiro serão os responsávei pelo suporte, as demais pessoas da facilitação estarão com atribuições específicas. Fique atenta ou atento ao andamento da atividade. Ajude a cuidar o tempo.
# Aquecimento dos grupos (10 minutos): Continue preparado para o suporte.
# Primeira rodada (20 minutos): E assim siga.
# Primeiro intervalo (10 minutos): Espere a pessoa cerimonialista passar a palavra a ti. Então, explique como funcionará a plataforma da interface de compartilhamento. Indique que podem ir dois integrantes de cada grupo (relatoria e mais um) por vez e eles devem ter em mãos toda a documentação. Na entrada, elas serão perguntadas sobre qual licença querem aplicar ao seu trabalho. Lá poderão compartilhar seu conteúdo livremente e conversar sobre os projetos, documentando o que for discutido. Mas a interface é um pouco longe e dá um pouco de trabalho de usar.
# Segunda rodada (20 minutos): Quando os participantes chegarem (no máximo dois por grupo), pergunte sob qual licença querem disponibilizar o trabalho. Explique o que significa cada uma:
## CC0 (ou domínio público): garantia de liberdade total sem restrições;
## CC BY (atribuição): garantia de liberdade se for feita referência às pessoas que desenvolveram o trabalho;
## CC BY-SA (compartilha igual): garantia de liberdade, com referência, impondo que quem usa o trabalho deve usar a mesma licença;
## CC BY-SA NC (não comercial): garantia de liberdade, com referência, compartilhando com mesma licença, impondo que não possa ser feito uso comercial se for usado o trabalho (desrecomende essa licença por não ser considerada livre, pois o conteúdo distribuído a aplicando não pode ser usado para serviços pagos posteriormente, muitas vezes limitando o acesso ao invés de ampliar);
## Ao final, deixe que decidam e entre o post it para colarem na folha de backup e disponibilizarem na mesa de publicação. Lembre de verificar se colocaram os nomes dos participantes do grupo no trabalho. Se não tiverem feito, disponibilize um post it para que o façam. Oriente para que mostrem os demais rascunhos do projetos aos outros grupos e conversem, e que para cada dúvida sanada, postem no fórum da plataforma a pergunta e a resposta como post its. Defina a cor dos post its de pergunta e resposta. Ao final dos 15 minutos oriente para que todos saiam e levem de volta as documentações;
# Segundo intervalo (20 minutos): Tente captar o que foi importante nas colaborações que presenciastes na plataforma com a pessoa cerimonialista para ajudar na avaliação.
# Última rodada (20 minutos): Ajude a percerber os avanços alcançados, relatando a pessoa cerimonialista. Especialmente naqueles grupos pelos quais ela já passou e conversou com a referência.
# Fechamento (5 minutos): Fique atenta ou atento no suporte do encerramento.

*Referências:* Mostre-se alguém simpático mais bastante meticuloso, e um tanto confuso. Tente passar o espírito de que a plataforma pode de uso não muito fácil, um pouxo exigente, com alguns pontos soltos, mas que garante a liberdade das pessoas.

*Material Extra:* Cartilha das Licenças Creative Commons

* CC0 (ou domínio público): garantia de liberdade total sem restrições;
* CC BY (atribuição): garantia de liberdade se for feita referência às pessoas que desenvolveram o trabalho;
* CC BY-SA (compartilha igual): garantia de liberdade, com referência, impondo que quem usa o trabalho deve usar a mesma licença;
* CC BY-SA NC (não comercial): garantia de liberdade, com referência, compartilhando com mesma licença, impondo que não possa ser feito uso comercial se for usado o trabalho (licença não livre, desrecomendada).

______________________________

*Pessoa Facilitadora: Mensageiro*

*Atribuições:*

    Essa pessoa vai passar por todos os grupos se comunicando com as pessoas, às vezes falando coisas que fazem sentido e às vezes só besteira. Então deve ser alguém bem comunicativo ou comunicativa, que esteja disposta a fazer palhaçada e falar coisas que façam as pessoas se sentirem bem para distrair elas. Passará boa parte da atividade no suporte, então deve ser alguém bem observador ou observadora. Ṕortanto, é alguém com visão intermediária sobre a atividade.


*Atribuições por etapa:*
    
-1: Articulação das pessoas facilitadoras (30 minutos): Ajude a montar as ilhas de trabalho e separe seus post its. Então preste ajuda às pessoas da facilitação que estiverem precisando mais. Ajude a resolver dúvidas que ainda permaneçam.
0. Formação dos grupos (10 minutos): Ajude a distribuir os identificadores de grupo.
# Abertura (10 minutos): Nesse momento, tu e a interface serão as responsávei pelo suporte, pois as demais pessoas da facilitação estarão com atribuições específicas. Fique atenta ou atento ao andamento da atividade. Ajude a cuidar o tempo.
# Aquecimento dos grupos (10 minutos): Continue preparado para o suporte.
# Primeira rodada (20 minutos): E assim siga.
# Primeiro intervalo (10 minutos): Espere a pessoa cerimonialista lhe passar a palavra então explique como funcionará a plataforma do mensageiro. Diga que elas terão a oportunidade de mandar mensagens para os outros grupos sem nem sair do lugar, que o mensageiro se encarregará de levar as mensagens, que poderão ser escritas em post its que tu distribuirá a elas.
# Segunda rodada (20 minutos): Comece a passar de grupo em grupo, e pergunte se querem participar da plataforma. Diga que para isso terão de concordar com os "termos e condições de uso". Então apresente. Os termos e condições de uso devem ser um texto muito grande com frases do tipo "Concordo em ser zuado pela plataforma" e "Concordo que a plataforma disponibilize informações que não condizem com as informações publicadas originalmente" bem pequeno ao final. Bote pressão para que as pessoas assinem sem ler. Então as pessoas devem assinar a  concordância com os termos e condições de uso. Então ganham dois post its onde podem postar mensagens aos outros grupos. Faça esse processo em todos os grupos. Então recolha as mensagens e comece a circular aleatoriamente pelos grupos. Mostre de um a quatro post its por vez, de maneira aleatoria. Sempre que mostrar, leia também. As vezes mostre rapidamente, ás vezes vagarosamente. Mostre a ideia de que elas e eles não tem controle sobre ti. Alterne as mensagens dos grupos com mensagens de propaganda, ou de notícia, ou claramente caricaturescas de que querem induzir a fazer algo ou alguma forma de pensamento. Talvez algo oposto aos prórprios ODS. Seja criativa ou criativo. Puxe papo aleatoriamente com alguma pessoa do grupo enquanto estiver mostrando as mensagens para confundi-la. Dê mais atenção a alguns grupos do que a outros. Quando encerrarem os 15 minutos, pare de interagir com os grupos.
# Segundo intervalo (20 minutos): Passe para a pessoa cerimonialista detalhes que atrapalharam na colaboração dos grupos e no resultado final dentro do que observastes para ajudar na avaliação.
# Última rodada (20 minutos): Ajude a percerber os avanços alcançados, relatando a pessoa cerimonialista. Especialmente naqueles grupos pelos quais ela já passou e conversou com a referência.
# Fechamento (5 minutos): Fique atenta ou atento no suporte do encerramento.


*Referências:* Palhaço ou pessoa tagarela, ou um misto dos dois.

______________________________

*Pessoa Facilitadora: Referências/Monitoria*

*Atribuições:*

    Essa pessoa deve ficar responsável por um grupo, então deve estar disposta a ajudar e ficar atenta. Idealmente, deve ter uma expressão teatral boa para contagiar o grupo a entrar nos personagens. Podem ser pessoas menos familiarizadas com a atividade.


*Atribuições por etapa:*
    
-1: Articulação das pessoas facilitadoras (30 minutos): Tire dúvidas que ainda tenham permanecido com pessoas com visão mais clara sobre a dinâmica. Separe os materiais indicadores do grupo para o número de participantes estabelecido. Coloque o seu e informe a pessoa cerimonialista quanto ao grupo pelo serás responsável. Misture os indicadores de seu grupo junto com o de outros grupos. Coloque o  identificador da mesa do grupo pelo qual és responsável. Confira se o kit de trabalho já está presente.
0. Formação dos grupos (10 minutos): Distribua juntamente com as outras referências os objetos indicadores para os participantes. Informe a pessoa cerimonialista quando concluir.
# Abertura (10 minutos): Coloque-se de maneira visível e com alguma distância até as demais referências para quando a cerimonialista anunciar seu grupo possas ser facilmente identificada.
# Aquecimento dos grupos (10 minutos): Garanta que o número indicado de participantes chegou até ti e conduza o grupo para sua ilha de trabalho. Explique a qual setor da sociedade o grupo corresponde e apresente as características do grupo, espaços de fala/expressão/poder, espaço de interlocução com outros setores, potencialidades e algumas limitações. Foque nas potencialidades ao invés de limitações. Tenha em mente que o objetivo é provocar o espírito de que eles são representates desse setor no grupo. Tente interpretar uma caricatura de personagem que remeta a esse grupo para tentar inspirar as pessoas do grupo. Oriente também para a definição de um mediador e um relator. O mediador deve focar em garantir que todas as pessoas do grupo tenham garantida a sua fala. Dê exemplos, se conseguir. Lembre de que é importante de que todes falem e oriente a mediadora a perguntar a opinião daquelas e daqueles que estão mais calados se apenas algumas pessoas estiverem falando. Indique que a relatora é quem faz as anotações de solução, garantindo a documentação. Oriente essa pessoa para que ela possibilite que outras pessoas também registrem, mas deve zelar pela folha de documentação. Lembre para que todos os registros sejam feitos com o papel vegetal embaixo e a outra folha especial, para que seja feito o backup. Ao final, pergunte se ainda restam dúvidas e tente saná-las.
# Primeira rodada (20 minutos): Fique próxima ao seu grupo para verificar se está tudo correndo bem. Tenha especial atenção as pessoas responsáveis pela mediação e pelo registro. Cuide o tempo. Quando faltarem 10 e 5 minutos, indique ao grupo. Resolva dúvidas pontuais, mas estimule também a autonomia. Encontre a medida certa. Peça ajuda ao cerimonialista, a inferface e ao algoritmo em caso de necessidade. Se estiverem indisponíveis, procure outra referência para pedir ajuda. Confira o cronômetro regularmente. Quando faltar 1 minuto, avise de maneira mais enfática de que o tempo está se acabando.
# Primeiro intervalo (10 minutos): Separe as novas folhas de documentação para o teu grupo e ajude a garantir que o grupo está atento as orientações que estão sendo passadas.
# Segunda rodada (20 minutos): Entregue as novas folhas de documentação. Avise que ao final desse tempo a pessoa mediadora terá 2 minutos para explicar a solução encontrada, portanto deve estar atenta ao que foi registrado. Nos primeiros 15 minutos mantenha a prática da primeira rodada, agora esclarecendo também sobre as questões de comunicação. Nos últimos 5 minutos, garanta que todas as pessoas do grupo estão presetes e oriente de maneira enérgica que terminem de documentar.
# Segundo intervalo (20 minutos): Chame a pessoa mediadora para acompanhá-la até a cerimonialista. Fique atento ou atenta às apresentação e discuta brevemente com outras facilitadoras. Separe as novas folhas de documentação.
# Última rodada (20 minutos): Distribua um novo conjunto de folhas de documentação. Ajude o grupo a interagir de maneira produtiva e focada, mantendo a autonomia do grupo. Confira quais avanços estão sendo atingidos e repasse ao cerimonialista quando ele chegar ao grupo.
# Fechamento (5 minutos): Ajude a reunir as pessoas do teu grupo em um grande círculo para o fechamento da atividade.


_________________________________

*GRUPOS:* artistas, representates do legislativo e executivo, professores, pesquisadoras, profissionais de informática, profissionais de engenharia, profissionais especialistas em meio ambiente, agricultores.

_________________________________

*Grupo 1:* Artistas

    Para a referência: Se for para fazer uma caricatura, aja, fale, se expresse de maneira apaixonada.


*Descrição:* Uma categoria muito ampla, que envolve desde rappers a artistas plástic@s, passando por grafiteir@s, músic@s de diversos estilos, poetas, atrizes/atores, curadoras/curadores, equilibristas, malabaristas, palhaç@s.
Espaços de fala/expressão/poder: Acesso privilegiado a alguns veículos da mídia, universidades, canais de comunicação digital de amplo acesso. Grandes manifestações, podem atrair visibilidade ao seu trabalho pela capacidade comover, ou dar voz a iniciativas por meio da sua arte.
*Interlocuções:* Com o grande público, reconhecidos por pessoas comuns.
Potencialidades: Alta sensibilidade, paixão pelo que é feito, crença de que a arte é algo importante para o mundo, estudos sobre aspectos sociais, visão densa sobre o mundo.
*Limitações:* Falta de recursos. Quanto menos popular é a arte, muitas vezes se tem menos acesso aos recursos ou espaços de poder.
    
_________________________________

*Grupo 2:* Representantes do legislativo e executivo

    Para a referência: Se for para fazer uma caricatura, aja de maneira um pouco enrolada  e buscando agradar aos outros e atingir popularidade.


*Descrição:* Um grupo de pessoas com bastante poder. Podem ter formas distintas de atuação. Se for um representante do legislativo, pode propor leis e se dedicar para conseguir acordos com colegas que aprovar estas leis. Tomam decisões sobre orçamento do governo, incentivos a algumas atividades, bem como outras questões sociais, seja em termos de educação, saúde, transporte ou outros. Se for um representante do executivo, pode definir como executar o orçamento do governo, quais programas implementar, dentro de normas estabelecidas pelo legislativo. Podem ser de diferentes abrangências também, desde a municipal, onde a princípio se tem maior contato com as pessoas, até a escala nacional, onde aumenta o distanciamento.
*Espaços de fala/expressão/poder:* As próprias instâncias do estado na qual são representates. Proposição de leis e implementação de programas de governo, em diferentes escalas.
*Interlocuções:* Podem  propor interlocuções com diversos grupos da sociedade que certamente haverá alguma abertura, mas normalmente ficam boa parte do tempo interagindo entre si, prática sustentadora do ofício.
*Potencialidades:* Capacidade de intervir nas estruturas e recursos do estado. Visibilidade junto à população.
*Limitações:* Comprometimento com financiadores de campanha. Ambiente estagnado e com baixa propensão à mudança nas estruturas. Visibilidade junto à população.

_________________________________

*Grupo 3:* Professoras e Professores

    Para a referência: Se for para fazer uma caricatura, assuma ar professoral, fale pausademente, com clareza, pacíência e segurança;


*Descrição:* Categoria presente na vida de quase todas as pessoas. Podem ter diversas áreas de especialização, desde geografia à gestão, passando por matemática, linguagens, ciências exatas, dentre muitas outras. Podem ter diferentes ambientes de atuação, como escolas - que ainda podem ser estaduais, municipais, federais ou privadas - ou Universidades, ou institutos de formação técnica, ou numa relação direta com o educando, em formatos de aula particular. Podem também ter diferentes formas de atuação, como lecionando matérias de maneira teórica ou prática, atuando na gestão de espaços educacionais ou na elaboração de recursos educacionais.
*Espaços de fala/expressão/poder:* As próprias salas de aula, na qual tem contato privilegiado com diversas pessoas. Associações de professores. Cargos de diretoria. Comissões de avaliação.
Interlocuções: Com muitas e muitos jovens, com pais e mães, com uma grande comunidade de colegas.
*Potencialidades:* São reconhecidos como elemento fundamental da sociedade por quase todes. São respeitados em diversos espaços. Tem amplo acesso a conhecimento e treinamento em comunicação com pessoas. Cada professor interage com um grupo muito grande de pessoas e tem um tempo de interação garantido para si.
*Limitações:* Recursos disponíveis. Muitas vezes os professores ficam sobrecarregados de trabalho e não recebem os devidos recursos financeiros e de trabalho para atuarem. Falta estrutura física nas escolas. Muitas vezes também falta segurança, alimentação, dentre outros. Com isso, o ambiente de atuação muitas está estagnado e desesperançoso.

_________________________________

*Grupo 4:* Pesquisadoras e Pesquisadores

    Para a referência: Se for para fazer uma caricatura, fale de maneira entusiasmada e um pouco confusa, buscando uso de jargões.


*Descrição:* Um grupo muito amplo de pessoas. Podem atuar na pesquisa, ou também na gestão e avaliação da pesquisa. Atuam em diversas áreas, desde ciências sociais à física, e em suas diversas subáreas. Podem atuar em institutos de pesquisa ou Universidades, que podem ser espaços públicos ou privados. Muitas vezes são também educadoras e educadores.
*Espaços de fala/expressão/poder:* Universidades, salas de aula, direções de instituto, comissões de avaliação, corpos editoriais de publicações, chefias de laboratório, conselhos de pesquisa e agências de financiamento. Laudos e pesquisas que embasam decisões de outros setores da sociedade. Desenvolvimento de tecnologia que impacta interações sociais.
*Interlocuções:* Entre si, nos diferentes institutos e laboratórios, normalmente com uma posição de autoridade. Alguma interlocução com representantes do legislativo e executivo e veículos de mídia.
Potencialidades: Amplo acesso a conhecimento, muitas vezes denso e profundo, por anos de estudo. Entusiasmo pelo que se estuda. Senso critico maturado no ofício da ciência. Capacidade de gerar conhecimento ou artefatos de impacto social amplo.
*Limitações:* Baixa interlocução com diferentes setores da sociedade. Limitações a alguns nichos de pesquisa. Baixo incentivo a algumas áras do conhecimento. Pensamento monotônico. Baixa interação com perspectivas diversas.

_________________________________

*Grupo 5:* Profissionais de informática

    Para a referência: Se for para fazer uma caricatura, fale de maneira rápida e apaixonada, com bastante entusiasmo pelo que se está falando.


*Descrição:* Profissionais que trabalham, dominam e desenvolvem recursos da área de informação em tecnologia digital. Trabalham com ferramentas virtuais, rodadas em dispositivos computacionais. Podem ser desenvolvedores, inventando novas aplicações ou integradores de solução, implementando soluções existentes para diversas demandas.
*Espaços de fala/expressão/poder:* Aplicação de ferramentas das quais dispõe, que podem impactar muitas pessoas, ou impactar de maneira importante a vida de algumas pessoas, ou ambas as coisas combinadas.
*Interlocuções:* Com muitos grupos da sociedade, pois hoje todes precisam desses recursos.
*Potencialidades:* Alta capacidade de desenvolver ferramentas. Amplo acesso a conhecimento específico. Forte influência na vida das pessoas.
*Limitações:* Falta de condição de diálogo mais profundo com muitas das pessoas impactadas, bem como falta de condições de acesso a um conhecimento e reflexões mais amplas, como aspectos sociais.
    
_________________________________

*Grupo 6:* Profissionais de engenharia

    Para a referência: Se for para fazer uma caricatura, aja de maneira muito prática e às vezes um pouco ríspida.


*Descrição:* Grupo de pessoas amplo, de várias especialidades, que aplicam, desenvolvem e estudam ferramentas. Influenciam no trabalho e na vida de maneira mais geral de diversas pessoas. Podem atuar em sistemas mecânicos ou eletrônicos, ou ambos, com diversas finalidades: moradia, transporte, saúde, educação, manutenção, construção, comunicação.
Espaços de fala/expressão/poder: Execução de projetos e desenvolvimento de tecnologias que impactam na vida das pessoas. Laudos e avaliações que influenciam decisões políticas. Gestão de empresas que influenciam a sociedade.
*Interlocuções:* Com diversos setores do mercado, que impactam na implementação e financiamento de projetos. Universidades.
*Potencialidades:* Amplo acesso a conhecimentos específicos por anos de estudo. Capacidade de produzir soluções. Capacidade metódica. Experiência com projetos.
*Limitações:* Falta de contato com o grande público impactado por suas obras. Falta de condição de acesso a conhecimento mais amplo, como aspectos sociais.

_________________________________

*Grupo 7:* Profissionais especialistas em meio ambiente

    Para a referência: Se for para fazer uma caricatura, aja de maneira apaixonada e com um tom entre o atencioso e o revolucionário.


*Descrição:* Pessoas especialistas em diversos temas, seja quanto à biodiversidade - como vegetais, animais, fungos ou bactérias - seja quanto ao meio ambiente no qual estão inseridos e o qual compõe, como a atmosfera, as águas e a terra. Podem atuar como pesquisadoras/pesquisadores, ativistas, empreendedoras/empreendedores, educadoras/educadoras.
*Espaços de fala/expressão/poder:* Diversificados. Salas de aula. Manifestações políticas. Gestão de órgãos governamentais e organizações não governamentais. Estudos que impactam a sociedade. Restauradores de sistemas biológicos.
*Interlocuções:* Plural e dispersa.
*Potencialidades:* Amplo estudo da realidade que nos cerca e a qual compomos, a natureza. Capacidade de intervir nos recursos naturais. Ampla inserção em diferentes áreas de atuação, como educação, articulação política, gestão pública, pesquisa. Paixão pelo tema de estudo.
*Limitações:* Atuação dispersa, sem uma identidade muito clara. Padecem de dificuldades específicas das áreas de atuação e às vezes tem atuação limitada na preservação e restauração de sistemas naturais, por pressão política.

_________________________________

*Grupo 8:* Agricultoras e Agricultores

    Para a referência: Se for para fazer uma caricatura, aja de maneira simples e levemente desconfortável com espaços de fala, de maneira inibida.


*Descrição: Pessoas comuns e de grande sabedoria de interação com a terra, aprendida de maneira prática e de influência forte sobre a vida, por meio da alimentação. Estão fortemente ligadas ao seu espaço, sua terra. Tem um conhecimento local e de sistemas da natureza profundo.
*Espaços de fala/expressão/poder:* Feiras, associações, movimentos sociais e influência nas vida das pessoas pelo alimento que produzem.
*Interlocuções:* Pessoas comuns e de todas as áreas, aqueles que se alimentam. Interações com professores e biológos.
*Potencialidades:* Capacidade de entender e agir sobre a natureza e produzir alimentos. Influência na alimentação e saúde das pessoas. Sensibilidade, capacidade de observação e gestão de recursos. Capacidade de aprender e ensinar.
*Limitações:* Baixo reconhecimento do seu saber por diversos setores da sociedade. Falta de recursos financeiros e articulação para impacto ainda mais forte na sociedade. Disputas pela terra com setores da sociedade que podem oferecer riscos a si mesmas e mesmos.


