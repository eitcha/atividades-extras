# Atividades Extras

Um conjunto de atividades educacionais que a princípio não fazem parte do ciclo de oficinas da EITCHA!


## Dinâmica: As Soluções, o Registro (as Bolhas) e a Rede.

Nesta proposta, estudantes serão reunid@s em grupos que simulem papeis sociais distintos, como
professores, legisladores, artistas, empresári@s para chegarem numa solução para um dos objetivos
do desenvolvimento sustentavel da ONU. A partir de então serão provocadas e provocados a registrarem a
solução encontrada da melhor forma, compartilharem suas soluções e exercerem a reflexão crítica
sobre diferentes elementos da tecnologia digital e suas interações com a sociedade.

Esta recurso educacional está disponibilizado abertamente sobre a licença [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.pt_BR) (Atribuição-CompartilhaIgual) e é de autoria de Leonardo Sehn, com a contribuição de Jan Luc Tavares e Cristthian Arpino, no âmbito de uma colaboração entre a [EITCHA!](http://eitcha.org), a Escola Itinerante de Tecnologia Cidadã Hacker, e o [CTA IF/UFRGS](http://cta.if.ufrgs.br), Centro de Tecnologia Acadêmica do Instituto de Física da UFRGS.
